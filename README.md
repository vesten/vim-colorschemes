# vim-colorschemes

A collection of favorite color schemes for Vim and Neovim.

# vim-colorschemes
A collection of favorite color schemes for Vim and Neovim.

I'm not the author of any of these themes. If you use any of these color schemes, please see and respect the various LICENSEs and COPYRIGHTs of the various authors.
